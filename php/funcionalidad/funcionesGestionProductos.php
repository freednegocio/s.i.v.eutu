<?php
/*
FUNCIONES PARA INGRESAR O BORRAR DATOS
*/
function registrarCategoria($conexion, $nombre)
{
	//DML: INSERT INTO tabla (atributos) VALUES (valores)
	/*Primero definimos el INSERT con los datos necesarios.
	NOTA: Esta funcion tambien debe utilizarse en conjunto con
	buscarIdPorEmail para obtener la ID del user en cuestion*/
	$dml = "INSERT INTO categorias (nombreCategoria) VALUES ('";
	$dml .= $nombre . "');" ;
	if ($conexion->query($dml) === TRUE)
	{ //Si el INSERT es exitoso
		echo "Categoria insertada con éxito!"; //AVISAR CON ECHO
	}
	else
	{ //Y si todo sale mal... Avisar.
		echo "Error: " . $dml . "<br>" . $conexion->connect_error;
	}		
}
function registrarProducto($conexion, $nombre, $stock, $precioUnidad, $descripcion, $idCategoria)
{
	//DML: INSERT INTO tabla (atributos) VALUES (valores)
	/*Primero definimos el INSERT con los datos necesarios.
	NOTA: Esta funcion tambien debe utilizarse en conjunto con
	buscarIdPorEmail para obtener la ID del user en cuestion*/
	$dml = "INSERT INTO productos (nombre, stock, precioUnidad, descripcion, idCategoria) VALUES ('";
	$dml .= $nombre . "',". $stock .",". $precioUnidad .", '". $descripcion ."',". $idCategoria .");" ;
	if ($conexion->query($dml) === TRUE)
	{ //Si el INSERT es exitoso
		echo "Categoria insertada con éxito!"; //AVISAR CON ECHO
	}
	else
	{ //Y si todo sale mal... Avisar.
		echo "Error: " . $dml . "<br>" . $conexion->connect_error;
	}		
}
/*
function borrarCategoria($conexion, $id)
{
	*/
	/*Primero definimos el INSERT con los datos necesarios.
	NOTA: Esta funcion tambien debe utilizarse en conjunto con
	buscarIdPorEmail para obtener la ID del user en cuestion*/
/*	$sql = "DELETE FROM categorias WHERE idCategoria =". $id .");";
	if ($conexion->query($dml) === TRUE)
	{ //Si el INSERT es exitoso
		echo "Categoria insertada con éxito!"; //AVISAR CON ECHO
	}
	else
	{ //Y si todo sale mal... Avisar.
		echo "Error: " . $dml . "<br>" . $conexion->connect_error;
	}		
}
*/
/*
FUNCIONES PARA BUSCAR DATOS
*/
function contarProductosCategoria($conexion, $idCategoria)
{
	//Con esta función contamos la cantidad de tablas de productos de la categoria X...
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para productos de X categoria.
	$sql = "SELECT * FROM productos WHERE idCategoria = '" . $idCategoria . "'";
	$resultado = $conexion->query($sql);
	return $resultado->num_rows;
}
function getNombresCategoria($conexion)
{
	//Con esta función obtenemos el nombre de una categoria...
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para buscar el nombre de la categoria.
	$sql = "SELECT * FROM categorias";
	$resultado = $conexion->query($sql);
	if ($resultado->num_rows > 0)
	{ // ^^ Si existen filas con ese EMAIL
		while ($fila = $resultado->fetch_assoc()){
			echo "<option value='". $fila['idCategoria'] ."'>". $fila['nombreCategoria'] . "</option>";
		}
	}
	else
	{
		echo "Error en la consulta de busqueda de id."; //Si todo sale mal, avisar
	}
}
function getIdCategoria($conexion, $nombreCategoria)
{
	//Con esta función obtenemos el nombre de una categoria...
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para buscar el nombre de la categoria.
	$sql = "SELECT * FROM categorias WHERE nombreCategoria = '" . $nombreCategoria . "'";
	$resultado = $conexion->query($sql);
	if ($resultado->num_rows > 0)
	{ // ^^ Si existen filas con ese EMAIL
		$fila = $resultado->fetch_assoc(); //Guardarla en la variable fila
		return $fila["idCategoria"]; //Y tomar el atributo nombre
	}
	else
	{
		echo "Error en la consulta de busqueda de id."; //Si todo sale mal, avisar
	}
}
function getDatosProducto($conexion, $idProducto)
{
	//Con esta función obtenemos el nombre de un producto...
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para buscar el nombre del producto.
	$sql = "SELECT * FROM productos WHERE idProducto = '" . $idProducto . "'";
	$resultado = $conexion->query($sql);
	if ($resultado->num_rows > 0) //si existen filas con esa id
	{
		$fila = $resultado->fetch_assoc(); //guardarlas en el array fila
		$filas = array();
		$filas[0] = $fila["nombre"];
		$filas[1] = $fila["descripcion"];
		$filas[2] = $fila["precioUnidad"];
		$filas[3] = $fila["tipoMoneda"];
		$filas[4] = $fila["stock"];
		return $filas; //tomar el atributo nombre
	}
	else
	{
		echo "Error en la consulta de busqueda de id."; //Si todo sale mal, avisar
	}
}
function contarCategorias($conexion)
{
	//Con esta función contamos las categorias que existen en la bd.
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para contar las tablas.
	$sql = "SELECT * FROM categorias";
	$resultado = $conexion->query($sql);
	//Devolvemos el numero de tablas
	return $resultado->num_rows;
}
function contarProductos($conexion)
{
	//Con esta función contamos las categorias que existen en la bd.
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para contar las tablas.
	$sql = "SELECT * FROM productos";
	$resultado = $conexion->query($sql);
	//Devolvemos el numero de tablas
	return $resultado->num_rows;
}
function contarProductosPorCategoria($conexion, $idCategoria)
{
	//Con esta función contamos las categorias que existen en la bd.
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para contar las tablas.
	$sql = "SELECT * FROM productos";
	$resultado = $conexion->query($sql);
	//Devolvemos el numero de tablas
	return $resultado->num_rows;
}
function mostrarCategorias($conexion){
	//SQL: SELECT * FROM nombreTabla
	$sql = "SELECT * FROM categorias";
	
	$resultado = $conexion->query($sql);

	if ($resultado->num_rows > 0){ //hay más de una fila en el resultado?s
		while ($fila = $resultado->fetch_assoc()){//avanzar a la próxima fila
			echo "<tr>";
			echo "<td>" . $fila["idCategoria"] ."</td>";
			echo "<td>" . $fila["nombreCategoria"] ."</td>";
			/*/echo "<td><a href=\"paneles/altabajamodiCategoria.php?borrar=" . $fila["idCategoria"] ."\" >X</a></td>";
			echo "<td><a href=\"formularioBD.php?id=" . $fila["idCategoria"] ."\" >M</a></td>";*/
			echo "</tr>";
		}
		echo "</table>";
	}

}

function mostrarProductos($conexion){
	//SQL: SELECT * FROM nombreTabla
	$sql = "SELECT * FROM productos";
	
	$resultado = $conexion->query($sql);

	if ($resultado->num_rows > 0){ //hay más de una fila en el resultado?
		while ($fila = $resultado->fetch_assoc()){//avanzar a la próxima fila
			echo "<tr>";
			echo "<td>" . $fila["idProducto"] ."</td>";
			echo "<td>" . $fila["nombre"] ."</td>";
			echo "<td>" . $fila["precioUnidad"] ."</td>";
			echo "<td>" . $fila["stock"] ."</td>";
			echo "<td>" . $fila["tipoMoneda"] ."</td>";
			echo "<td>" . $fila["descripcion"] ."</td>";
			echo "<td>" . $fila["idCategoria"] ."</td>";
			/*/echo "<td><a href=\"paneles/altabajamodiCategoria.php?borrar=" . $fila["idCategoria"] ."\" >X</a></td>";
			echo "<td><a href=\"formularioBD.php?id=" . $fila["idCategoria"] ."\" >M</a></td>";*/
			echo "</tr>";
		}
		echo "</table>";
	}

}

?>