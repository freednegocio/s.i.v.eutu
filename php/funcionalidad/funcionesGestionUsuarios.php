<?php
/*
FUNCIONES PARA INGRESAR DATOS
*/
function registrarUsuario($conexion, $datos)
{
	//DML: INSERT INTO tabla (atributos) VALUES (valores)
	//PRIMERO DEFINIMOS EL DML PARA INSERTAR EN LA TABLA USUARIOS
	/*
		Aquel 27 místico al final del DML, es la variable de "tipo de usuario",
		en otros scripts es chequeada. Existe con el fin de evitar que un user
		acceda a un panel que no debería.
	*/
	$dml = "INSERT INTO usuarios (nombre, apellido, email, contraseña, tipo) VALUES";
	$dml .= "('". $datos[0] . "','" . $datos[1] . "','" . $datos[2] . "',PASSWORD('" . $datos[3] . "'), 27);";

	if ($conexion->query($dml) === TRUE){ //SI EL INSERT SALE EXITOSO
		echo "Usuario registrado (1/3)"; //ECHO PARA DEBUG...
	}
	else
	{
		echo "Error: " . $dml . "<br>" . $conexion->connect_error; //MOSTRAR ERROR SI SALE MAL
	}		
}
function registrarCliente($conexion, $id, $datos){
	//DML: INSERT INTO tabla (atributos) VALUES (valores)
	/*Primero definimos el INSERT con los datos necesarios.
	NOTA: Esta funcion tiene que utilizarse en conjunto con
	buscarIdPorEmail para obtener la ID del user en cuestion*/
	$dml = "INSERT INTO clientes (idUsuario, calle, numeroPuerta, barrio, puntosPromo) VALUES (";
	$dml .= $id . ",'" . $datos[4] . "','" . $datos[5] . "','" . $datos[6] . "', 0);" ;
	if ($conexion->query($dml) === TRUE)
	{ //Si el INSERT es exitoso
		echo "Cliente registrado (2/3)"; //AVISAR CON ECHO
	}
	else
	{ //Y si todo sale mal... Avisar.
		echo "Error: " . $dml . "<br>" . $conexion->connect_error;
	}		
}
function registrarNumeroCliente($conexion, $id, $datos)
{
	//DML: INSERT INTO tabla (atributos) VALUES (valores)
	/*Primero definimos el INSERT con los datos necesarios.
	NOTA: Esta funcion tambien debe utilizarse en conjunto con
	buscarIdPorEmail para obtener la ID del user en cuestion*/
	$dml = "INSERT INTO clitel (idUsuario, telefono) VALUES (";
	$dml .= $id . "," . $datos[7] . ");" ;
	if ($conexion->query($dml) === TRUE)
	{ //Si el INSERT es exitoso
		echo "Telefono de cliente registrado (3/3)"; //AVISAR CON ECHO
	}
	else
	{ //Y si todo sale mal... Avisar.
		echo "Error: " . $dml . "<br>" . $conexion->connect_error;
	}		
}
/*
FUNCIONES PARA BUSCAR DATOS
*/
function buscarIdPorEmail($conexion, $email)
{
	//Con esta función obtenemos la id con el email de un usuario...
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para buscar la ID de un usuario a través de su EMAIL.
	$sql = "SELECT * FROM usuarios WHERE email = '" . $email . "'";
	$resultado = $conexion->query($sql);
	if ($resultado->num_rows > 0)
	{ // ^^ Si existen filas con ese EMAIL
		$fila = $resultado->fetch_assoc(); //Guardarlas en el array fila
		return $fila["idUsuario"]; //Y tomar el atributo idUsuario
	}
	else
	{
		echo "Error en la consulta de busqueda de id."; //Si todo sale mal, avisar
	}
}
function getNombreApellido($conexion, $id)
{
	//Con esta función obtenemos nombre y apellido del user de la ID dada...
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para buscar los datos del user.
	$sql = "SELECT * FROM usuarios WHERE idUsuario = '" . $id . "'";
	$resultado = $conexion->query($sql);
	if ($resultado->num_rows > 0)
	{ // ^^ Si existen filas con esa id
		$fila = $resultado->fetch_assoc(); //Guardarlas en el array fila
		$filas = array(); //Crear un array
		$filas[0]=$fila['nombre']; //almaceno nombre
		$filas[1]=$fila['apellido'];//y apellido en él
		return $filas; //Y devuelvo el array al sistema para utilizar los datos
	}
	else
	{
		echo "Error en la consulta de busqueda de id."; //Si todo sale mal, avisar
	}
}
function getEmail($conexion, $id)
{
	//Con esta función obtenemos el Email a traves de la ID de un user.
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para buscar el email de un usuario a traves de su id.
	$sql = "SELECT * FROM usuarios WHERE idUsuario = '" . $id . "'";
	$resultado = $conexion->query($sql);
	if ($resultado->num_rows > 0)
	{ // ^^ Si existen filas con esa id
		$fila = $resultado->fetch_assoc(); //Guardarlas en el array fila
		return $fila["email"]; //Y tomar el atributo email
	}
	else
	{
		echo "Error en la consulta de busqueda de id."; //Si todo sale mal, avisar
	}
}
function compararEmail($conexion, $email)
{
	//Con esta función verificamos si está registrado el mail en la bd
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para buscar el EMAIL.
	$sql = "SELECT * FROM usuarios WHERE email = '" . $email . "'";
	$resultado = $conexion->query($sql);
	if ($resultado->num_rows > 0)
	{ // ^^ Si existen filas con ese EMAIL
		return true;
	}
	else
	{
		return false;
	}
}
function compararContra($conexion, $email, $pw)
{
	//Con esta función comparamos la contraseña de la BD con la ingresada por el user en el login.
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para buscar la pw.
	$sql = "SELECT * FROM usuarios WHERE email = '" . $email . "' AND contraseña = PASSWORD('" . $pw . "')";
	$resultado = $conexion->query($sql);
	if ($resultado->num_rows > 0)
	{ // ^^ Si existen filas con ese EMAIL
		return true;
	}
	else
	{
		return false; //Si todo sale mal, avisar
	}
}
function getTipo($conexion, $id)
{
	//Con esta función obtenemos el tipo de usuario...
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para buscar al usuario por id.
	$sql = "SELECT * FROM usuarios WHERE idUsuario = '" . $id . "'";
	$resultado = $conexion->query($sql);
	if ($resultado->num_rows > 0)
	{ // ^^ Si existen filas con esa ID
		$fila = $resultado->fetch_assoc(); //Guardarlas en el array fila
		return $fila["tipo"]; //Y tomar el atributo tipo
	}
	else
	{
		echo "Error en la consulta de busqueda de id."; //Si todo sale mal, avisar
	}
}

function getDireccion($conexion, $id)
{
	//Con esta función obtenemos la direccion de un cliente...
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para buscar la direccion del cliente a traves de su id.
	$sql = "SELECT * FROM clientes WHERE idUsuario = '" . $id . "'";
	$resultado = $conexion->query($sql);
	if ($resultado->num_rows > 0)
	{ // ^^ Si existen filas con ese EMAIL
		$fila = $resultado->fetch_assoc(); //Guardarlas en el array fila
		$filas = array();
		$filas[0] = $fila['calle'];
		$filas[1] = $fila['numeroPuerta'];
		$filas[2] = $fila['barrio'];
		return $filas; //Y tomar el atributo idUsuario
	}
	else
	{
		echo "Error en la consulta de busqueda de id."; //Si todo sale mal, avisar
	}
}

?>