<?php
		//Inicio el array datos, para almacenar el input del user
		$datos[0] = "iniciado";
		function cancelarScript(){
			//redirigir
			header('location: ../paginas/registro.php');
			//Y detener script
			die();
		}
		function mostrarError($mensaje){
			if(!(session_status() == PHP_SESSION_ACTIVE))
			{
			session_start();
			}
			$_SESSION['msjError'] = $mensaje;
		}
		//NOMBRE 0
		if(isset($_POST['nombre'])) //Si se recibe el nombre del formulario
		{
			//Agregar al array para insertar en bd
			$datos[0] = $_POST['nombre'];
		}else{
			//Si no se recibe
			//Mostrar mensaje de error
			mostrarError("Buen intento, pillo.");
			//detener el script
			cancelarScript();
		}
		//APELLIDO 1
		if(isset($_POST['apellido'])) //Si se recibe el apellido del formulario
		{
			//Agregar al array para insertar en bd
			$datos[1] = $_POST['apellido'];
		}else{
			//Si no se recibe
			//Mostrar mensaje de error
			mostrarError("Buen intento, pillo.");
			//detener el script
			cancelarScript();
		}
		//EMAIL 2 
		if(isset($_POST['email'])) //Si se recibe el email del formulario
		{
			//Agregar al array para insertar en bd
			$datos[2] = $_POST['email'];
		}else{
			//Si no se recibe
			//Mostrar mensaje de error
			mostrarError("Buen intento, pillo.");
			//detener el script
			cancelarScript();
		}
		//CONTRASEÑA 3
		//Si la contraseña fue definida en el formulario
		if(isset($_POST['contra']) && isset($_POST['contraConfi']))
		{
			//Y coincide la contraseña con la confirmación
			if($_POST['contra'] == $_POST['contraConfi'])
			{
				//Agregar datos al array para sumarlos a la bd
				$datos[3] = $_POST['contra'];
			}else{
				//Si no se recibe
				//Mostrar mensaje de error
				mostrarError("ERROR: ¡Las contraseñas no coinciden!");
				//detener el script
				cancelarScript();
			}
		}
		//CALLE 4
		if(isset($_POST['calle'])) //Si se recibe la calle del formulario
		{
			//Agregar al array para insertar en bd
			$datos[4] = $_POST['calle'];
		}else{
			//Si no se recibe
			//Mostrar mensaje de error
			mostrarError("Buen intento, pillo.");
			//detener el script
			cancelarScript();
		}
		//N PUERTA 5
		if(isset($_POST['numeroPuerta'])) //Si se recibe el numero de puerta del formulario
		{
			//Agregar al array para insertar en bd
			$datos[5] = $_POST['numeroPuerta'];
		}else{
			//Si no se recibe
			//Mostrar mensaje de error
			mostrarError("Buen intento, pillo.");
			//detener el script
			cancelarScript();
		}
		//BARRIO 6
		if(isset($_POST['barrio'])) //Si se recibe el barrio del formulario
		{
			//Agregar al array para insertar en bd
			$datos[6] = $_POST['barrio'];
		}else{
			//Si no se recibe
			//Mostrar mensaje de error
			mostrarError("Buen intento, pillo.");
			//detener el script
			cancelarScript();
		}
		//TELEFONO 7
		if(isset($_POST['telefono'])) //Si se recibe el telefono del formulario
		{
			//Agregar al array para insertar en bd
			$datos[7] = $_POST['telefono'];
		}else{
			//Si no se recibe
			//Mostrar mensaje de error
			mostrarError("Buen intento, pillo.");
			//detener el script
			cancelarScript();
		}
		//Si todo está en orden, insertar datos en la BD

		include("bdVisitante.php"); //Incluir funcionalidades para abrir conexion con privilegios de visitante.
		include("funcionesGestionUsuarios.php"); //Insertar datos del formulario en correspondientes tablas.
		$conexion = abrirConexion(); //Abrir conexion
		registrarUsuario($conexion,$datos); //Ingresar datos en tabla usuarios
		$id = buscarIdPorEmail($conexion,$datos[2]); //Buscar id de tabla usuario por email
		registrarCliente($conexion, $id, $datos); //Ingresar datos en tabla cliente segun id
		registrarNumeroCliente($conexion, $id, $datos); //Ingresar datos en tabla clitel segun id
		$tipo = getTipo($conexion, $id);
		cerrarConexion($conexion); //cerrar conexion
		//redirigir y terminar script
		if(!(session_status() == PHP_SESSION_ACTIVE))
		{
		session_start();
		}
		$_SESSION['idUsuario'] = $id;
		$_SESSION['tipo'] = $tipo;
		header("location:../paginas/principal.php");
		die();
		

?>
