<?php
		//Inicio el array datos, para almacenar el input del user
		$datos[0] = "iniciado";
		function cancelarScript(){
			//redirigir
			header('location: ../paginas/ingreso.php');
			//Y detener script
			die();
		}
		function mostrarError($mensaje){
			if(!(session_status() == PHP_SESSION_ACTIVE))
			{
				session_start();
			}
			$_SESSION['msjError'] = $mensaje;
		}
		//email 0
		if(isset($_POST['email'])) //Si se recibe el email del formulario
		{
			//Agregar al array para consultar en bd
			$datos[0] = $_POST['email'];
		}else{
			//Si no se recibe
			//Mostrar mensaje de error
			mostrarError("Buen intento, pillo.");
			//detener el script
			cancelarScript();
		}
		if(isset($_POST['contra'])) //Si se recibe el nombre del formulario
		{
			//Agregar al array para consultar en bd
			$datos[1] = $_POST['contra'];
		}else{
			//Si no se recibe
			//Mostrar mensaje de error
			mostrarError("Buen intento, pillo.");
			//detener el script
			cancelarScript();
		}
		
		include("bdCliente.php"); //Incluir funcionalidades para abrir conexion con privilegios de visitante.
		include("funcionesGestionUsuarios.php"); //Incluir funcionalidades para consultar las tablas de user.
		$conexion = abrirConexion(); //Abrir conexion

		if(compararEmail($conexion, $datos[0]))
			//Si el mail existe en la bd
		{
			if(compararContra($conexion, $datos[0], $datos[1]))
				//Comprobar si la contraseña coincide con la ingresada en la bd
			{
				//iniciar, redirigir y terminar script
				$id = buscarIdPorEmail($conexion,$datos[0]); //Buscar id de tabla usuario por email
				if(!(session_status() == PHP_SESSION_ACTIVE))
				{
					session_start();
				}
				$tipo = getTipo($conexion, $id);
				$_SESSION['tipo'] = $tipo;
				$_SESSION['idUsuario'] = $id;
				cerrarConexion($conexion);
				header("location:../paginas/principal.php");
				die();
			}
			else
			{
				//Si no coincide
				//Mostrar mensaje de error
				mostrarError("Contraseña incorrecta.");
				//detener el script
				cerrarConexion($conexion);
				cancelarScript();
			}
		}
		else
		{
			//Si no existe
			//Mostrar mensaje de error
			mostrarError("Esta email no está registrado.");
			//detener el script
			cerrarConexion($conexion);
			cancelarScript();
		}
?>
