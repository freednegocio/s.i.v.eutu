<?php
//Definición de las constantes para la conexión con la BD
define('SERVIDOR', "localhost");
define('USUARIO', "cliente");
define('CONTRA', "pruebacliente");
define('BD', "sivefreed");

function abrirConexion(){
	//Crear la conexión con MariaDB
	$conexion = @new mysqli(SERVIDOR, USUARIO, CONTRA, BD);

	//Verificar el estado de la conexión
	if ($conexion->connect_error){
		die("Error en la conexión con la BD: " . $conexion->connect_error);
	}else{
		return $conexion;
	}
}

function cerrarConexion($conexion){
	$conexion->close();
}
?>