<?php
/*
FUNCIONES PARA INGRESAR DATOS
*/

function registrarEmpresa($conexion, $nombre)
{
	//DML: INSERT INTO tabla (atributos) VALUES (valores)
	//PRIMERO DEFINIMOS EL DML PARA INSERTAR EN LA TABLA USUARIOS
	/*
		Aquel 61 místico al final del DML, es la variable de "tipo de usuario",
		en otros scripts es chequeada. Existe con el fin de evitar que un user
		acceda a un panel que no debería.
	*/
	$dml = "INSERT INTO empresas (nombre) VALUES";
	$dml .= "('". $nombre . "');";

	if ($conexion->query($dml) === TRUE){ //SI EL INSERT SALE EXITOSO
		echo "Empleado registrado en tabla Usuarios (1/3)"; //ECHO PARA DEBUG...
	}
	else
	{
		echo "Error: " . $dml . "<br>" . $conexion->connect_error; //MOSTRAR ERROR SI SALE MAL
	}		
}
function registrarUsuarioEmpleado($conexion, $datos)
{
	//DML: INSERT INTO tabla (atributos) VALUES (valores)
	//PRIMERO DEFINIMOS EL DML PARA INSERTAR EN LA TABLA USUARIOS
	/*
		Aquel 61 místico al final del DML, es la variable de "tipo de usuario",
		en otros scripts es chequeada. Existe con el fin de evitar que un user
		acceda a un panel que no debería.
	*/
	$dml = "INSERT INTO usuarios (nombre, apellido, email, contraseña, tipo) VALUES";
	$dml .= "('". $datos[0] . "','" . $datos[1] . "','" . $datos[2] . "',PASSWORD('" . $datos[3] . "'), 61);";

	if ($conexion->query($dml) === TRUE){ //SI EL INSERT SALE EXITOSO
		echo "Empleado registrado en tabla Usuarios (1/3)"; //ECHO PARA DEBUG...
	}
	else
	{
		echo "Error: " . $dml . "<br>" . $conexion->connect_error; //MOSTRAR ERROR SI SALE MAL
	}		
}
function registrarEmpleado($conexion, $id, $idEmpresa){
	//DML: INSERT INTO tabla (atributos) VALUES (valores)
	/*Primero definimos el INSERT con los datos necesarios.
	NOTA: Esta funcion tiene que utilizarse en conjunto con
	buscarIdPorEmail  y buscarIdEmpresa para obtener la ID 
	del user y la empresa en cuestion*/
	$dml = "INSERT INTO empleados (idUsuario, idEmpresa) VALUES (";
	$dml .= $id . "," . $idEmpresa . ");" ;
	if ($conexion->query($dml) === TRUE)
	{ //Si el INSERT es exitoso
		echo "Empleado registrado en tabla Empleados (2/2)"; //AVISAR CON ECHO
	}
	else
	{ //Y si todo sale mal... Avisar.
		echo "Error: " . $dml . "<br>" . $conexion->connect_error;
	}		
}
/*
FUNCIONES PARA BUSCAR DATOS
*/
function buscarIdEmpresa($conexion, $nombre)
{
	//Con esta función obtenemos la id, e insertamos datos en tablas de clientes, con la id de usuario...
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para buscar la ID de un usuario a través de su EMAIL.
	$sql = "SELECT * FROM empresas WHERE nombre = '" . $nombre . "'";
	$resultado = $conexion->query($sql);
	if ($resultado->num_rows > 0)
	{ // ^^ Si existen filas con ese EMAIL
		$fila = $resultado->fetch_assoc(); //Guardarla en la variable fila
		return $fila["idEmpresa"]; //Y tomar el atributo idUsuario
	}
	else
	{
		echo "Error en la consulta de busqueda de id."; //Si todo sale mal, avisar
	}
}

function mostrarEmpresas($conexion){
	//SQL: SELECT * FROM nombreTabla
	$sql = "SELECT * FROM empresas";
	
	$resultado = $conexion->query($sql);

	if ($resultado->num_rows > 0){ //hay más de una fila en el resultado?
		while ($fila = $resultado->fetch_assoc()){//avanzar a la próxima fila
			echo "<tr>";
			echo "<td>" . $fila["idEmpresa"] ."</td>";
			echo "<td>" . $fila["nombre"] ."</td>";
			/*/echo "<td><a href=\"paneles/altabajamodiCategoria.php?borrar=" . $fila["idCategoria"] ."\" >X</a></td>";
			echo "<td><a href=\"formularioBD.php?id=" . $fila["idCategoria"] ."\" >M</a></td>";*/
			echo "</tr>";
		}
		echo "</table>";
	}

}
function getNombresEmpresas($conexion)
{
	//Con esta función obtenemos el nombre de una categoria...
	//SQL: SELECT * FROM tabla WHERE ...
	//Definimos la consulta para buscar el nombre de la categoria.
	$sql = "SELECT * FROM empresas";
	$resultado = $conexion->query($sql);
	if ($resultado->num_rows > 0)
	{ // ^^ Si existen filas con ese EMAIL
		while ($fila = $resultado->fetch_assoc()){
			echo "<option value='". $fila['idEmpresa'] ."'>". $fila['nombre'] . "</option>";
		}
	}
	else
	{
		echo "Error en la consulta de busqueda de id."; //Si todo sale mal, avisar
	}
}
function mostrarEmpleados($conexion){
	//SQL: SELECT * FROM nombreTabla
	$sql = "SELECT * FROM empleados";
	
	$resultado = $conexion->query($sql);

	if ($resultado->num_rows > 0){ //hay más de una fila en el resultado?
		while ($fila = $resultado->fetch_assoc()){//avanzar a la próxima fila
			echo "<tr>";
			echo "<td>" . $fila["idUsuario"] ."</td>";
			echo "<td>" . $fila["idEmpresa"] ."</td>";
			/*/echo "<td><a href=\"paneles/altabajamodiCategoria.php?borrar=" . $fila["idCategoria"] ."\" >X</a></td>";
			echo "<td><a href=\"formularioBD.php?id=" . $fila["idCategoria"] ."\" >M</a></td>";*/
			echo "</tr>";
		}
		echo "</table>";
	}

}
?>