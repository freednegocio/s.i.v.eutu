<!DOCTYPE html>
<html>
<head>
	<title>S.I.V.E</title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
	<link rel="stylesheet" href="../../css/estilos.css">
</head>
<body style="font-family: sans-serif">
<header>
	<div class="logo">
		<img src="../../img/logo.png" alt="FREEDMARKET">
	</div>
</header>
<?php
			$id=-1;
			$tipo=-1;
			$miPerfil = true;
			if(!(session_status() == PHP_SESSION_ACTIVE))
			{
				session_start();
			}
			if(isset($_SESSION['idUsuario']) && !isset($_GET['user'])) //Si no hay una sesión con la id del user
			{
				$id=$_SESSION['idUsuario'];
			}
			else if(isset($_GET['user']) && !isset($_SESSION['idUsuario']) )
			{
				$miPerfil = false;
				$id=$_GET['user'];
			}
			else if($_GET['user']==$_SESSION['idUsuario'])
			{
				$id=$_SESSION['idUsuario'];
			}
			else if($_GET['user']!=$_SESSION['idUsuario'])
			{
				$miPerfil = false;
				$id=$_GET['user'];
			}
			else
			{
				header("location:principal.php");
			}
			if(isset($_SESSION['tipo']))
			{
				$tipo=$_SESSION['tipo'];
			}
			include("../funcionalidad/revisarPrivilegios.php");
			include("../funcionalidad/funcionesGestionUsuarios.php");
			revisarPrivilegios($tipo);
			

			$conexion = abrirConexion();
			$nombreUser = getNombreApellido($conexion, $id)[0];
			$apellidoUser = getNombreApellido($conexion, $id)[1];
			$emailUser = getEmail($conexion, $id);
			$tipoUser = getTipo($conexion, $id);
			$esAdmin = esAdmin($tipoUser);
			if(!$esAdmin)
			{
			$calleUser = getDireccion($conexion, $id)[0];
			$numeroUser = getDireccion($conexion, $id)[1];
			$barrioUser = getDireccion($conexion, $id)[2];
			}
			cerrarConexion($conexion);

?>
<nav>
	<ul>
		<li class="item">
			<a href="principal.php">
				<div>INICIO</div>
			</a>
		</li>
		<li class="item">
			<a href="categorias.php">
				<div>CATEGORÍAS</div>
			</a>
		</li>
		<li class="item">
			<a href="productos.php">
					<div>PRODUCTOS</div>
			</a>
		</li>
			<?php
				if($id == -1)
				{
					echo "
					<li class='item'>
						<a href='ingreso.php'>
							<div>INGRESAR</div>
						</a>
					</li>
					<li class='item'>
						<a href='registro.php'>
							<div>REGISTRARME</div>
						</a>
					</li>
					";
				}else{
					if($id==$_SESSION['idUsuario']){
					echo "
					<li class='item'>
						<a href='#'>
							<u><strong>
								<div>MI PERFIL</div>
							</u></strong>
						</a>
					</li>
					";
					}else{
					echo "
					<li class='item'>
						<a href='perfil.php'>
							<div>MI PERFIL</div>
						</a>
					</li>
					";
					}
					echo 
					"
					<li class='item'>
						<a href='../funcionalidad/cerrarSesion.php'>
							<div>Cerrar sesión</div>
						</a>
					</li>
					";
				}
				if($id>0){
					$tipo = $_SESSION['tipo'];				
					if(esAdmin($tipo));
					{
						echo
						"
						<li class='item'>
							<a href='admin/ingreso.php'>
								<div>ADMIN PANEL</div>
							</a>
						</li>
						";
					}
				}
			?>
	</ul>
</nav>
<section class="seccion">
		<article class="cajaIngreso">
		
				<div class="contenFormulario">
					<?php
					$info =
					"
					<div>
						<img src='../../img/fotoPerfil.png'>
						<h1>".$nombreUser." ".$apellidoUser."</h1>
					";
					if($esAdmin)
					{
						$info.=
						"
						<p>
							ADMIN<br>
						</p>
						";
					}
					$info.= "
						<p>
							email: ". $emailUser ."<br>
						</p>
						<table border='1' style='width: 100%;'>
						<tr><td>numero(s) de telefono</td></tr>
						<tr><td>A</td></tr>
						<tr><td>B</td></tr>
						<tr><td>C</td></tr>
						</table>
					</div>
					";
					if($miPerfil && !$esAdmin)
					{
						$info .=
						"
						<table border='1' style='width: 100%;'>
						<tr><td colspan='2'>Direccion</td></tr>
						<tr><td>Calle</td><td>". $calleUser ."</td></tr>
						<tr><td>Numero</td><td>". $numeroUser ."</td></tr>
						<tr><td>Barrio</td><td>". $barrioUser ."</td></tr>
						</table>
						<div>
							<h1>Opciones de usuario</h1>
							<a href='historial.php' class='textoBoton'><div class='botonSencillo'>Mi historial</div></a>
						</div>
						";
					}
					if(($miPerfil && !$esAdmin) || ($miPerfil && $esAdmin))
					{
						$info.=
						"
						<div class='contenFormulario'>
							<div>
							<br>
							<a href='comentarios.php' class='textoBoton'><div class='botonSencillo'>Mis comentarios</div></a>
							</div>
						</div>
						";
					}
					echo $info;
					?>
				</div>
		</article>
	</section>
<footer>
	<div>
		Derechos reservados FREED.
	</div>
</footer>
</body>
</html>