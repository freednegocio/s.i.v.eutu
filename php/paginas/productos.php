<!DOCTYPE html>
<html>
<head>
	<title>S.I.V.E</title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
	<link rel="stylesheet" href="../../css/estilos.css">
</head>
<body style="font-family: sans-serif">
<?php 
			$nombreUser = "visitante";
			$emailUser = "";
			$id = -1;
			if(!(session_status() == PHP_SESSION_ACTIVE))
			{
				session_start();
			}
			if(isset($_SESSION['idUsuario']) && isset($_SESSION['tipo'])) //Si hay una sesión con la id del user
			{
			    $id = $_SESSION['idUsuario']; //almacenar id en variable interna
			    $tipo = $_SESSION['tipo']; //almacenar tipo de usuario en variable interna
			    include("../funcionalidad/revisarPrivilegios.php");
			    revisarPrivilegios($tipo);
			}else{
				include("../funcionalidad/bdVisitante.php");
			}
			include("../funcionalidad/funcionesGestionProductos.php");
				//Incluir funciones de gestion de productos y categorias 
?>

<header>
	<div class="logo">
		<img src="../../img/logo.png" alt="FREEDMARKET">
	</div>
</header>
<nav>
	<ul>
		<li class="item">
			<a href="principal.php">
					<div>INICIO</div>
			</a>
		</li>
		<li class="item">
			<a href="categorias.php">
				<div>CATEGORÍAS</div>
			</a>
		</li>
		<li class="item">
			<a href="#">
				<u><strong>
					<div>PRODUCTOS</div>
				</strong></u>
			</a>
		</li>
			<?php
				$conexion = abrirConexion();
				if($id == -1)
				{
					echo "
					<li class='item'>
						<a href='ingreso.php'>
							<div>INGRESAR</div>
						</a>
					</li>
					<li class='item'>
						<a href='registro.php'>
							<div>REGISTRARME</div>
						</a>
					</li>
					";
				}else{
					echo "
					<li class='item'>
						<a href='perfil.php'>
							<div>MI PERFIL</div>
						</a>
					</li>
					<li class='item'>
						<a href='../funcionalidad/cerrarSesion.php'>
							<div>Cerrar sesión</div>
						</a>
					</li>
					";
				}
				if($id>0){
					$tipo = $_SESSION['tipo'];				
					if(esAdmin($tipo));
					{
						echo
						"
						<li class='item'>
							<a href='admin/ingreso.php'>
								<div>ADMIN PANEL</div>
							</a>
						</li>
						";
					}
				}
				cerrarConexion($conexion);
			?>
	</ul>
</nav>
	<section>
		<article class="contenedorProductos">
			<div class="titulo">
				<h1>Productos</h1>
			</div>
		<!--articulo 2-->
			<?php
			$conexion = abrirConexion();
			$cantProds = contarProductos($conexion);
			if($cantProds > 0)
			{
				do
				{
					echo
					"
					
					<article>
					<figure class='fotoProd'>
						<img src='../../img/producto.png'>
						<figcaption>PRECIO: ". getDatosProducto($conexion, $cantProds)[2] ." ". getDatosProducto($conexion, $cantProds)[3] ."</figcaption>
						<figcaption>STOCK: ". getDatosProducto($conexion, $cantProds)[4] ."</figcaption>
					</figure>
					<div class='nomDescProd'>
						<h1>". getDatosProducto($conexion, $cantProds)[0] ."</h1>
						
						<p>". getDatosProducto($conexion, $cantProds)[1] ."</p>
						<div class='LC'>
							<a href='producto.php?idprod=".$cantProds."'>¡Visitar pagina del producto!</a>
						</div>
					</div>
					</article>

					";
					$cantProds--;
				}while($cantProds>=1);
			}
			cerrarConexion($conexion);
			?>
		</article>
	</section>
<footer>
	<div>
		Derechos reservados FREED.
	</div>
</footer>
</body>
</html>