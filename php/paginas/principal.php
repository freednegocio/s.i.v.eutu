<!DOCTYPE html>
<html>
<head>
	<title>S.I.V.E</title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
	<link rel="stylesheet" href="../../css/estilos.css">
</head>
<?php 
			$nombreUser = "visitante";
			$emailUser = "";
			$id = -1;
			if(!(session_status() == PHP_SESSION_ACTIVE))
			{
				session_start();
			}
			if(isset($_SESSION['idUsuario']) && isset($_SESSION['tipo'])) //Si hay una sesión con la id del user
			{

			    $id = $_SESSION['idUsuario']; //almacenar id en variable interna
			    
			    $tipo = $_SESSION['tipo']; //almacenar tipo de usuario en variable interna
			    include("../funcionalidad/revisarPrivilegios.php");
			    revisarPrivilegios($tipo);
			}else{
				include("../funcionalidad/bdVisitante.php");
			}
			include("../funcionalidad/funcionesGestionUsuarios.php");
			$conexion = abrirConexion();
			if($id>0){
			$nombreUser = getNombreApellido($conexion, $id)[0];
			}
				//Incluir funciones de gestion de productos y categorias 
			
?>
<body style="font-family: sans-serif">
<header>
	<div class="logo">
		<img src="../../img/logo.png" alt="FREEDMARKET">
	</div>
</header>
<nav>
	<ul>
		<li class="item">
			<a href="#">
				<u><strong>
					<div>INICIO</div>
				</strong></u>
			</a>
		</li>
		<li class="item">
			<a href="categorias.php">
				<div>CATEGORÍAS</div>
			</a>
		</li>
		<li class="item">
			<a href="productos.php">
					<div>PRODUCTOS</div>
			</a>
		</li>
			<?php
				if($id == -1)
				{
					echo "
					<li class='item'>
						<a href='ingreso.php'>
							<div>INGRESAR</div>
						</a>
					</li>
					<li class='item'>
						<a href='registro.php'>
							<div>REGISTRARME</div>
						</a>
					</li>
					";
				}else{
					echo "
					<li class='item'>
						<a href='perfil.php'>
							<div>MI PERFIL</div>
						</a>
					</li>
					<li class='item'>
						<a href='../funcionalidad/cerrarSesion.php'>
							<div>Cerrar sesión</div>
						</a>
					</li>
					";
				}
				if($id>0){
					$tipo = $_SESSION['tipo'];				
					if(esAdmin($tipo));
					{
						echo
						"
						<li class='item'>
							<a href='admin/ingreso.php'>
								<div>ADMIN PANEL</div>
							</a>
						</li>
						";
					}
				}
				cerrarConexion($conexion);
			?>
	</ul>
</nav>
	<section class="seccion">

		<article class="cajaIngreso">
			<div class="titulo">
				<?php
					echo "<h1>Bienvenido $nombreUser! </h1>";
				?>
			</div>
		</article>
	</section>
	<?php
		if(!(session_status() == PHP_SESSION_ACTIVE))
		{
		session_start();
		}
		if(isset($_SESSION['msjError']))
		{
		    echo "<script type='text/javascript'>
		            alert('" . $_SESSION['msjError'] . "');
		          </script>";
		    //se termina la sesion para no mostrar el error despues de refrescar.
		    unset($_SESSION['msjError']);
		}
		?>
<footer>
	<div>
		Derechos reservados FREED.
	</div>
</footer>
</body>
</html>