<!DOCTYPE html>
<html>
<head>
	<title>S.I.V.E</title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
	<link rel="stylesheet" href="../../css/estilos.css">
</head>
<body style="font-family: sans-serif">
<?php 
			$nombreUser = "visitante";
			$emailUser = "";
			$id = -1;
			if(!(session_status() == PHP_SESSION_ACTIVE))
			{
				session_start();
			}
			if(isset($_SESSION['idUsuario']) && isset($_SESSION['tipo'])) //Si hay una sesión con la id del user
			{
			    $id = $_SESSION['idUsuario']; //almacenar id en variable interna
			    $tipo = $_SESSION['tipo']; //almacenar tipo de usuario en variable interna
			    include("../funcionalidad/revisarPrivilegios.php");
			    revisarPrivilegios($tipo);
			}else{
				include("../funcionalidad/bdVisitante.php");
			}
			include("../funcionalidad/funcionesGestionProductos.php");
				//Incluir funciones de gestion de productos y categorias 
?>

<header>
	<div class="logo">
		<img src="../../img/logo.png" alt="FREEDMARKET">
	</div>
</header>
<nav>
	<ul>
		<li class="item">
			<a href="principal.php">
					<div>INICIO</div>
			</a>
		</li>
		<li class="item">
			<a href="#">
				<u><strong>
					<div>CATEGORÍAS</div>
				</strong></u>
			</a>
		</li>
		<li class="item">
			<a href="productos.php">
					<div>PRODUCTOS</div>
			</a>
		</li>
			<?php
				$conexion = abrirConexion();
				if($id == -1)
				{
					echo "
					<li class='item'>
						<a href='ingreso.php'>
							<div>INGRESAR</div>
						</a>
					</li>
					<li class='item'>
						<a href='registro.php'>
							<div>REGISTRARME</div>
						</a>
					</li>
					";
				}else{
					echo "
					<li class='item'>
						<a href='perfil.php'>
							<div>MI PERFIL</div>
						</a>
					</li>
					<li class='item'>
						<a href='../funcionalidad/cerrarSesion.php'>
							<div>Cerrar sesión</div>
						</a>
					</li>
					";
				}
				if($id>0){
					$tipo = $_SESSION['tipo'];				
					if(esAdmin($tipo));
					{
						echo
						"
						<li class='item'>
							<a href='admin/ingreso.php'>
								<div>ADMIN PANEL</div>
							</a>
						</li>
						";
					}
				}
				cerrarConexion($conexion);
			?>
	</ul>
</nav>
	<section>
		<article class="cajaCategoria">
			<div class="titulo">
				<h3>Categorías</h3>
			</div>
			<?php
				$conexion = abrirConexion();
				$cantCat = contarCategorias($conexion);
				$contador = 4;
				if($cantCat>0){
					do{
						echo "<div>";
						do{
							$nombreCat = getNombreCategoria($conexion, $cantCat);
							
							$cantProductosCat = contarProductosCategoria($conexion, $cantCat);
							echo
							"
							<div class='categoria'>			
								<figure class='descripCategoria' align='center'>
									<img src='../../img/categoria.png'>
									<a href='categoria.php?cat=". $cantCat ."'>
										<figcaption>
											<strong>".$nombreCat." (". $cantProductosCat. ")</strong>
										</figcaption>
									</a>
								</figure>
							</div>
							";
							$contador--;
							$cantCat--;
						}while($contador>=1 && $cantCat>=1);
						echo "</div>";
					}while($cantCat>=1);
				}
				cerrarConexion($conexion);
			?>
		</article>
	</section>
<footer>
	<div>
		Derechos reservados FREED.
	</div>
</footer>
</body>
</html>