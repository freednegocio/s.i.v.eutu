<!DOCTYPE html>
<html>
<head>
	<title>S.I.V.E</title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
	<link rel="stylesheet" href="../../css/estilos.css">
	<script type="text/javascript">
		function ejemploManzana(){
			alert("EJEMPLO: Si su direccion termina en Manzana 41, Solar 10\n"+
				"Ingrese 4110 en el campo de numero de puerta");
		}
	</script>
</head>
<?php
if(!(session_status() == PHP_SESSION_ACTIVE))
{
session_start();
}
if(isset($_SESSION['idUsuario'])) //Si hay una sesión con la id del user
	{
		header("location: perfil.php");
	}
?>
<body style="font-family: sans-serif">
<header>
	<div class="logo">
		<img src="../../img/logo.png" alt="FREEDMARKET">
	</div>
</header>
	<nav>
		<ul>
			<li class="item">
				<a href="principal.php">
					<div>INICIO</div>
				</a>
			</li>
			<li class="item">
				<a href="categorias.php">
					<div>CATEGORÍAS</div>
				</a>
			</li>
			<li class="item">
				<a href="productos.php">
					<div>PRODUCTOS</div>
				</a>
			</li>
			<li class="item">
				<a href="ingreso.php">
					<div>INGRESAR</div>
				</a>
			</li>
			<li class="item">
				<a href="#">
					<strong><u>
						<div>REGISTRARME</div>
					</u></strong>
				</a>
			</li>
				<form class="item">
					<div>
						<input type="text" name="busqueda">
						<input type="submit" value="Buscar">
					</div>
				</form>
			</ul>
	</nav>
	<section class="seccion">
		<article class="cajaIngreso">
			<div class="titulo">
				<h1>Registro</h1>
			</div>
			<form method="POST" action="../funcionalidad/registrar.php">
				<div class="contenFormulario">
					<div>
						<h1>Datos personales</h1>
						<strong>Nombre *</strong><br>
						<input type="text" name="nombre" placeholder="Nombre" required maxlength="20"><br>
						<strong>Apellido</strong><br>
						<input type="text" name="apellido" placeholder="Apellido" required maxlength="20"><br>
						<strong>Email *</strong><br>
						<input type="email" name="email" placeholder="Email" required maxlength="30"><br>
						<strong>Contraseña *</strong><br>
						<input type="password" name="contra" placeholder="Contraseña" required maxlength="50"><br>
						<strong>Confirmar contraseña *</strong><br>
						<input type="password" name="contraConfi" placeholder="Confirmar contraseña" required maxlength="50"><br>
					</div>
				</div>
				<div class="contenFormulario">
					<div>
						<h1>Dirección</h1>
						<strong>Calle *</strong><br>
						<input type="text" name="calle" placeholder="Calle" required maxlength="30"><br>
						<strong>Numero de puerta *</strong><br>
						<input type="text" name="numeroPuerta" placeholder="Numero de puerta" required maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57'> <br>
						<strong>Barrio *</strong><br>
						<input type="text" name="barrio" placeholder="Barrio" required maxlength="25"> <br>
						<strong>Teléfono *</strong><br>
						<input type="tel" name="telefono" placeholder="09xxxxxxx" required maxlength="9"  onkeypress='return event.charCode >= 48 && event.charCode <= 57'> <br>
					</div>
				</div>
					<div>
					<h1><a onclick="ejemploManzana()" href="#">(¿Qué escribir si mi direccion contiene manzana y solar?)</a></h1>
					<input type="submit" value="Registrar">
					</div>
				</div>
			</form>
		</article>
		<?php
		if(!(session_status() == PHP_SESSION_ACTIVE))
		{
		session_start();
		}
		if(isset($_SESSION['msjError']))
		{
		    echo "<script type='text/javascript'>
		            alert('" . $_SESSION['msjError'] . "');
		          </script>";
		    //se termina la sesion para no mostrar el error despues de refrescar.
		    unset($_SESSION['msjError']);
		}
		?>
	</section>
<footer>
	<div>
		Derechos reservados FREED.
	</div>
</footer>
</body>
</html>