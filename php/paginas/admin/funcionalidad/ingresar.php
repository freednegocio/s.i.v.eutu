<!DOCTYPE html>
<html>
<head>
	<title>S.I.V.E</title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
	<link rel="stylesheet" href="css/estilos.css">
</head>
<?php
if(!(session_status() == PHP_SESSION_ACTIVE))
{
session_start();
}
if(isset($_SESSION['idUsuario']) && isset($_SESSION['tipo'])) //Si hay una sesión con la id del user
	{
		if($_SESSION['tipo']!=215)
		{
		header("location: ../principal.php");
		}
	}
?>
<body style="font-family: sans-serif">
<header>
	<div class="logo">
		<img src="../../../img/logoAdm.png" alt="FREEDMARKET">
	</div>
</header>
	<nav>
		<ul>
			<li class="item">
				<a href="principal.php">
					<div>Administrar<br>categorias</div>
				</a>
			</li>
			<li class="item">
				<a href="categorias.php">
					<div>Administrar<br>productos</div>
				</a>
			</li>
			<li class="item">
				<a href="productos.php">
					<div>Administrar<br>empresas</div>
				</a>
			</li>
			<li class="item">
				<a href="#">
					<u><strong>
						<div>Administrar<br>empleados</div>
					</strong></u>
				</a>
			</li>
			<li class="item">
				<a href="registro.php">
					<div>Administrar<br>usuarios</div>
				</a>
			</li>
	</nav>
	<section class="seccion">
		<article class="cajaIngreso">
			<div class="titulo">
				<h1>Inicio de sesión</h1>
			</div>
			<form method="POST" action="../../funcionalidad/ingresar.php">
				<strong>Email</strong><br>
				<input type="email" name="email" placeholder="Email" required><br>
				<strong>Contraseña</strong><br>
				<input type="password" name="contra" placeholder="Contraseña" required> <br>
				<input type="submit" value="Ingresar">
			</form>
		</article>
		<?php
		if(!(session_status() == PHP_SESSION_ACTIVE))
		{
		session_start();
		}
		if(isset($_SESSION['msjError']))
		{
		    echo "<script type='text/javascript'>
		            alert('" . $_SESSION['msjError'] . "');
		          </script>";
		    //se termina la sesion para no mostrar el error despues de refrescar.
		    unset($_SESSION['msjError']);
		}
		?>
	</section>
<footer>
	<div>
		Derechos reservados FREED.
	</div>
</footer>
</body>
</html>