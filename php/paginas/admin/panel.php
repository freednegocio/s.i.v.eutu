<!DOCTYPE html>
<html>
<head>
	<title>S.I.V.E</title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
	<link rel="stylesheet" href="css/estilos.css">
</head>
<?php
if(!(session_status() == PHP_SESSION_ACTIVE))
{
session_start();
}
if(isset($_SESSION['idUsuario']) && isset($_SESSION['tipo'])) //Si hay una sesión con la id del user
	{
		$tipo = $_SESSION['tipo'];
		include("../../funcionalidad/revisarPrivilegios.php");
		$esAdmin = esAdmin($tipo);
		revisarPrivilegios($tipo);
		if(!$esAdmin)
		{
		header("location: ../principal.php");
		}
	}
	$opt=-1;
	if(isset($_GET['opt']))
	{
		$opt=$_GET['opt'];
	}
?>
<body style="font-family: sans-serif">
<header>
	<div class="logo">
		<img src="../../../img/logoAdm.png" alt="FREEDMARKET">
	</div>
</header>
	<nav>
		<ul>
			<li class="item">
				<a href="panel.php?opt=1">
					<div>Administrar<br>categorias</div>
				</a>
			</li>
			<li class="item">
				<a href="panel.php?opt=2">
					<div>Administrar<br>productos</div>
				</a>
			</li>
			<li class="item">
				<a href="panel.php?opt=3">
					<div>Administrar<br>empresas</div>
				</a>
			</li>
			<li class="item">
				<a href="panel.php?opt=4">
						<div>Administrar<br>empleados</div>
				</a>
			</li>
			<li class="item">
				<a href="../../funcionalidad/cerrarSesion.php">
					<div>Cerrar<br>sesión</div>
				</a>
			</li>
	</nav>
	<section class="seccion">
		
		<?php
		$conexion = abrirConexion();
		switch ($opt) {
			case '1':
				include("paneles/admCategorias.php");
				break;
			case '2':
				include("paneles/admProductos.php");
				break;
			case '3':
				include("paneles/admEmpresas.php");
				break;
			case '4':
				include("paneles/admEmpleados.php");
				break;
			default:
				echo "HOLA!";
				break;
		}
		
		if(!(session_status() == PHP_SESSION_ACTIVE))
		{
		session_start();
		}
		if(isset($_SESSION['msjError']))
		{
		    echo "<script type='text/javascript'>
		            alert('" . $_SESSION['msjError'] . "');
		          </script>";
		    //se termina la sesion para no mostrar el error despues de refrescar.
		    unset($_SESSION['msjError']);
		}
		?>
	</section>
<footer>
	<div>
		Derechos reservados FREED.
	</div>
</footer>
</body>
</html>