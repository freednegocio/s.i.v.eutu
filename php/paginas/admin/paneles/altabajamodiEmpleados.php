<?php
include("../../../funcionalidad/funcionesGestionEmpresaEmpleado.php");	
include("../../../funcionalidad/funcionesGestionUsuarios.php");
include("../../../funcionalidad/bdAdm.php");
$conexion = abrirConexion();
$datos[0] = "iniciado";
if(isset($_POST['nombre']))
{
	$datos[0] = $_POST['nombre'];
}else{
	header("location: ../../principal.php");
	die();
}	
if(isset($_POST['apellido']))
{
	$datos[1] = $_POST['apellido'];
}else{
	header("location: ../../principal.php");
	die();
}	

if(isset($_POST['email']))
{
	$datos[2] = $_POST['email'];
}else{
	header("location: ../../principal.php");
	die();
}	

if(isset($_POST['contra']))
{
	$datos[3] = $_POST['contra'];
}else{
	header("location: ../../principal.php");
	die();
}	
if(isset($_POST['idEmpresa']))
{
	$datos[4] = $_POST['idEmpresa'];
}else{
	header("location: ../../principal.php");
	die();
}	
registrarUsuarioEmpleado($conexion, $datos[0], $datos[1], $datos[2], $datos[3]);
$id = buscarIdPorEmail($conexion, $datos[2]);
registrarEmpleado($conexion, $id, $datos[4]);
cerrarConexion($conexion);
header("location: ../panel.php?opt=2");
	
?>
