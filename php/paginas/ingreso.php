<!DOCTYPE html>
<html>
<head>
	<title>S.I.V.E</title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
	<link rel="stylesheet" href="../../css/estilos.css">
</head>
<?php
if(!(session_status() == PHP_SESSION_ACTIVE))
{
session_start();
}
if(isset($_SESSION['idUsuario'])) //Si hay una sesión con la id del user
	{
		header("location: perfil.php");
	}
?>
<body style="font-family: sans-serif">
<header>
	<div class="logo">
		<img src="../../img/logo.png" alt="FREEDMARKET">
	</div>
</header>
	<nav>
		<ul>
			<li class="item">
				<a href="principal.php">
					<div>INICIO</div>
				</a>
			</li>
			<li class="item">
				<a href="categorias.php">
					<div>CATEGORÍAS</div>
				</a>
			</li>
			<li class="item">
				<a href="productos.php">
					<div>PRODUCTOS</div>
				</a>
			</li>
			<li class="item">
				<a href="#">
					<u><strong>
						<div>INGRESAR</div>
					</strong></u>
				</a>
			</li>
			<li class="item">
				<a href="registro.php">
					<div>REGISTRARME</div>
				</a>
			</li>
				<form class="item">
					<div>
						<input type="text" name="busqueda">
						<input type="submit" value="Buscar">
					</div>
				</form>
			</ul>
	</nav>
	<section class="seccion">
		<article class="cajaIngreso">
			<div class="titulo">
				<h1>Inicio de sesión</h1>
			</div>
			<form method="POST" action="../funcionalidad/iniciarSesion.php">
				<strong>Email</strong><br>
				<input type="email" name="email" placeholder="Email" required><br>
				<strong>Contraseña</strong><br>
				<input type="password" name="contra" placeholder="Contraseña" required> <br>
				<input type="submit" value="Ingresar">
				<p><strong>¿No estás registrado?</strong></strong><br><a href="registro.php">¡Haz click aquí!</a></p>
			</form>
		</article>
		<?php
		if(!(session_status() == PHP_SESSION_ACTIVE))
		{
		session_start();
		}
		if(isset($_SESSION['msjError']))
		{
		    echo "<script type='text/javascript'>
		            alert('" . $_SESSION['msjError'] . "');
		          </script>";
		    //se termina la sesion para no mostrar el error despues de refrescar.
		    unset($_SESSION['msjError']);
		}
		?>
	</section>
<footer>
	<div>
		Derechos reservados FREED.
	</div>
</footer>
</body>
</html>